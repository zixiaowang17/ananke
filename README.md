# Ananke

Visit the [website](https://ananke.readthedocs.io) to find out more.

[Ananke](https://en.wikipedia.org/wiki/Ananke), named for the Greek
primordial goddess of necessity and causality, is a python package for
causal inference using the language of graphical models

## Contributors

* Rohit Bhattacharya 
* Jaron Lee
* Razieh Nabi 
* Preethi Prakash
* Ranjani Srinivasan


## Installation

### Install graphviz

If graphing support is required, it is necessary to install [graphviz](https://www.graphviz.org/download/).

Ubuntu:
```shell script
sudo apt install graphviz
```

Mac ([Homebrew](https://brew.sh/)):
```shell script
brew install graphviz
```

If on M1 see this [issue](https://github.com/pygraphviz/pygraphviz/issues/398). The fix is to run the following before installing:
```shell script
brew install graphviz
python -m pip install \
    --global-option=build_ext \
    --global-option="-I$(brew --prefix graphviz)/include/" \
    --global-option="-L$(brew --prefix graphviz)/lib/" \
    pygraphviz
```

Fedora:
```shell script
sudo yum install graphviz
```

### Install ananke
Requires python3.7+. To install, run

```shell script
pip3 install ananke-causal
```
